# index.py

import os
import coverage

from flask_script import Manager, Server

COV = coverage.coverage(
    branch=True,
    include='application/*',
    omit=[  
        'application/config/config.py', 
    ]
)
COV.start()

from application import app 

manager = Manager(app)

if __name__ == '__main__':
    manager.add_command('runserver', Server(host='0.0.0.0'))
    manager.run()

