import sys
import site

site.addsitedir('/python-project/python-detect-text-in-image/env/lib/python3.9/site-packages')

sys.path.insert(0, '/python-project/python-detect-text-in-image')

from application import app