# Python Detect Text in Image

1. Pastikan sudah terinstall python 3.xx dan pip 

2. Clone Repo
```
$ git clone https://gitlab.com/erwinbudiantoro/python-detect-text-in-image.git
```


3. CD ke folder project
```
$ cd python-detect-text-in-image
```

4. Buat environment 
```
$ python3 -m venv env
```


5. Aktifkan environment
```
$ source env/bin/activate
```


6. Install requirements 
```
(env) pip install -r init/requirements.txt
```

7. RUN
```
(env) python3 index.py runserver
```
    
8. Buka postman dan buat request ke URL 'http://127.0.0.1:5000/convert' dengan method POST, dan form-data [file] dengan value sebuah gambar berformat jpg/png 
