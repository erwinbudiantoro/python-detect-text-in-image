# application/config/config.py

import os
basedir = os.path.abspath(os.path.dirname(__file__)) 


class BaseConfig:
    """Base configuration."""
    SECRET_KEY = 'ValarMorghulis'
    SIGNATURE = 'assalamualaykum'
    DEBUG = False
    BCRYPT_LOG_ROUNDS = 13
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(BaseConfig):
    """Development configuration."""
    DEBUG = True   
 
