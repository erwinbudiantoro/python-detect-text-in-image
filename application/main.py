# application/main.py
import os
from flask import Blueprint, request, make_response, jsonify
from flask.views import MethodView

try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract

main_blueprint = Blueprint('main', __name__)
 
class Index(MethodView):
    def get(self):
        final_response = {
                    'status': True,
                    'code': 200,
                    'message': 'Hello',
                    'data': 'From the otter slide'
                    }
        return make_response(jsonify(final_response)), 200

class ConvertImage(MethodView): 
    def post(self):
        # check if the post request has the file part
        if 'file' not in request.files:
            final_response = {
                'status': False,
                'code': 204,
                'message': 'Parameter yang dikirim salah',
                'data': None
                }
        else: 
            file = request.files['file']
            # if user does not select file, browser also
            # submit a empty part without filename
            if file.filename == '':
                final_response = {
                    'status': False,
                    'code': 204,
                    'message': 'Foto tidak terdeteksi',
                    'data': None
                    }
            else:
                # call the OCR function on it
                file.save(os.path.join(os.getcwd() + '/storage/upload/', file.filename))
                extracted_text = pytesseract.image_to_string(Image.open(file))   
                if extracted_text == ' \n\f':
                    response_text = 'Text tidak terdeteksi'
                    status = False
                    status_code = 202
                else :
                    response_text = extracted_text
                    status = True
                    status_code = 200

                final_response = {
                    'status': status,
                    'code': status_code,
                    'message': 'Konversi Img ke String',
                    'data': response_text
                    }

        return make_response(jsonify(final_response)), 200       

# define the API resources 
convert_image_view = ConvertImage.as_view('convert_image_api') 
index_view = Index.as_view('index_api') 

# add Rules for API Endpoints
main_blueprint.add_url_rule('/convert',view_func=convert_image_view,methods=['POST']) 
main_blueprint.add_url_rule('/welcome',view_func=index_view,methods=['GET']) 
