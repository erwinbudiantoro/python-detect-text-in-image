# application/__init__.py

import os

from flask import Flask 

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app_settings = os.getenv(
    'APP_SETTINGS',
    'application.config.config.DevelopmentConfig'
)
app.config.from_object(app_settings)
 
from application.main import main_blueprint
app.register_blueprint(main_blueprint)
